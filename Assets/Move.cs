using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    Rigidbody rb;
    private float speed = 2.0f;
    private float acceleration = 0.0f;
    private float slowdown = 0.0f;
    private float horizontalInput;
    private float turnSpeed = 115.0f;
    private float forwardInput;
    public float maxSpeed = 0.5f; //������������ ��������
    public float sideSpeed = 0f; //������� ��������

    float dirX;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    float speed_delta = 0;
    float accel_delta = 0;
    float slowdown_delta = 0;
    float current_speed = 0;

    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        forwardInput = Input.GetAxis("Vertical");

        speed_delta = forwardInput;

        accel_delta = Input.GetKey(KeyCode.Mouse1) ? 0.2f : 0.1f;
        slowdown_delta = Input.GetKey(KeyCode.Space) ? -5.0f : 0.0f;
        //slowdown_delta = slowdown_delta == 0.0f && speed_delta == 0.0f ? -2.0f : speed_delta;
        //acceleration = slowdown >= 0 ? acceleration : 0.0f;

        current_speed += speed_delta * accel_delta + slowdown_delta;
        current_speed = Mathf.Max(current_speed, 5);
        transform.Translate(Vector3.forward * Time.deltaTime * current_speed * forwardInput);
        transform.Rotate(Vector3.up, turnSpeed * horizontalInput * Time.deltaTime);
        if (horizontalInput != 0)
        {
            sideSpeed = horizontalInput * -1f; //���� ����� ����� �� ��������� ����� ��� ������, ����� ������� ��������
        }

        if (forwardInput != 0)
        {
            speed += 0.01f * forwardInput; //���� ����� ����� ����� ��� ����
        }
        else //���� ����� �� ����� �� �����, �� ����, �� �������� ����� ���������� ������������ � ����
        {
            if (speed > 0)
            {
                speed -= 0.01f;
            }
            else
            {
                speed += 0.01f;
            }
        }

        if (speed > maxSpeed)
        {
            speed = maxSpeed; //�������� �� ���������� ������������ ��������
        }

    }
    void FixedUpdate()
    {
        //rb.velocity = new Vector3(dirX, rb.velocity.x);
    }
}
